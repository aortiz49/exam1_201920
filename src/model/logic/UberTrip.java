/*
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * University of the Andes
 * Department of Systems Engineering
 * Licensed under Academic Free License version 2.1
 * Lab 2: Stacks & Queues
 * Author: Andy Ortiz
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 */

package model.logic;


/**
 * Class that represents an Uber trip.
 */
public class UberTrip {
    // -------------------------------------------------------------
    // Attributes
    // -------------------------------------------------------------
    private int sourceId;
    private int destinationId;
    private int hourOfDay;
    private float meanTravelTime;
    private float stdDevTravelTime;
    private float geoMeanTravelTime;
    private float geoStdDevTravelTime;
    // -------------------------------------------------------------
    // Constructor
    // -------------------------------------------------------------
    /**
     * Creates a new Uber trip.
     *
     * @param pSourceId            Id corresponding to the origin zone.
     * @param pDestinationId       Id corresponding to the destination zone.
     * @param pHourOfDay           Hour in which the trip took place (24 hour time)
     * @param pMeanTravelTime      Average time on the trip in seconds.
     * @param pStdDevTravelTime    Standard deviation of the travel time.
     * @param pGeoMeanTravelTime   Geometric average of the travel time.
     * @param pGeoStdDevTravelTime Geometric standard deviation of the travel time.
     */
    public UberTrip(int pSourceId, int pDestinationId, int pHourOfDay, float pMeanTravelTime,
                    float pStdDevTravelTime, float pGeoMeanTravelTime, float pGeoStdDevTravelTime) {
        sourceId = pSourceId;
        destinationId = pDestinationId;
        hourOfDay = pHourOfDay;
        meanTravelTime = pMeanTravelTime;
        stdDevTravelTime = pStdDevTravelTime;
        geoMeanTravelTime = pGeoMeanTravelTime;
        geoStdDevTravelTime = pGeoStdDevTravelTime;
    }
    // -------------------------------------------------------------
    // Methods
    // -------------------------------------------------------------
    /**
     * Returns the trip's origin source id.
     *
     * @return Trip's origin source id.
     */
    public int getSourceId() {
        return sourceId;
    }

    /**
     * Returns the trip's travel time.
     *
     * @return Trip's travel time.
     */
    public int getHourOfDay() {
        return hourOfDay;
    }

    /**
     * Returns the trip's destination id.
     *
     * @return Trip's destination source id.
     */
    public int getDestinationId() {
        return destinationId;
    }

    /**
     * Returns the trip's average travel time.
     *
     * @return Trip's average travel time;
     */
    public float getMeanTravelTime() {
        return meanTravelTime;
    }

    /**
     * Returns the trip's standard deviation for travel time.
     *
     * @return Trip's standard deviation for travel time.
     */
    public float getStdDevTravelTime() {
        return stdDevTravelTime;
    }

    /**
     * Returns the trip's geometric average travel time.
     *
     * @return Trip's geometric average travel time;
     */
    public float getGeoMeanTravelTime() {
        return geoMeanTravelTime;
    }

    /**
     * Returns the trip's geometric standard deviation for travel time.
     *
     * @return Trip's geometric standard deviation for travel time.
     */
    public float getGeoStdDevTravelTime() {
        return geoStdDevTravelTime;
    }
}
