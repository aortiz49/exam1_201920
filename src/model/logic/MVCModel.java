/*
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * University of the Andes
 * Department of Systems Engineering
 * Licensed under Academic Free License version 2.1
 * Lab 2: Stacks & Queues
 * Author: Andy Ortiz & Steven Rodriguez
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 */
package model.logic;
// -------------------------------------------------------------
// Imports
// -------------------------------------------------------------

import com.opencsv.CSVReader;
import model.data_structures.LinkedQueue;
import model.data_structures.LinkedStack;
import model.data_structures.Node;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * Definition of the world model
 */
public class MVCModel {
    // -------------------------------------------------------------
    // Attributes
    // -------------------------------------------------------------

    /**
     * A stack used in the MVCModel.
     */
    private LinkedStack<UberTrip> stack;

    /**
     * A queue used in the MVCModel.
     */
    private LinkedQueue<UberTrip> queue;

    /**
     * Uber trip representing the first trip in the stack.
     */
    private UberTrip firstTrip;

    /**
     * Uber trip representing the first trip in the stack.
     */
    private UberTrip lastTrip;

    // -------------------------------------------------------------
    // Constructor
    // -------------------------------------------------------------

    /**
     * Constructor of the world model with an empty stack.
     */
    public MVCModel() {
        stack = new LinkedStack<>();
        queue = new LinkedQueue<>();
    }

    // -------------------------------------------------------------
    // Methods
    // -------------------------------------------------------------

    /**
     * Returns the first trip in the stack.
     *
     * @return The first trip in the stack.
     */
    public UberTrip getFirstTrip() {
        return firstTrip;
    }

    /**
     * Returns the last trip in the stack.
     *
     * @return The last trip in the stack.
     */
    public UberTrip getLastTrip() {
        return lastTrip;
    }

    /**
     * Returns the size of the stack.
     *
     * @return The size of the stack.
     */
    public int size() {
        return stack.size();
    }

    /**
     * Loads a list of uber trips from csv files.
     */
    public void loadUberTrip() {

        String file1 = "data/bogota-cadastral-2018-1-All-HourlyAggregate.csv";
        //String file1 = "data/test.csv";

        // Load Uber trips from first file
        readCSV(file1);
    }

    /**
     * Reads the CSV file.
     *
     * @param pFileName File to be loaded.
     */
    private void readCSV(String pFileName) {
        long start = System.currentTimeMillis();
        try {

            // Create the streams
            FileInputStream inputStream = new FileInputStream(pFileName);
            InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
            BufferedReader bufferedReader = new BufferedReader(inputStreamReader);

            // Create a new CSV reader
            CSVReader reader = new CSVReader(bufferedReader);

            String[] nextLine;
            while ((nextLine = reader.readNext()) != null) {
                // Eliminate first line if it contains the headers
                if (nextLine[0].equals("sourceid")) {
                    nextLine = reader.readNext();
                }

                // Assign parsed values
                int sourceId = Integer.parseInt(nextLine[0]);
                int destinationID = Integer.parseInt(nextLine[1]);
                int hourOfDay = Integer.parseInt(nextLine[2]);
                float meanTravelTime = Float.parseFloat(nextLine[3]);
                float stdDevTravelTime = Float.parseFloat(nextLine[4]);
                float geoMeanTravelTime = Float.parseFloat(nextLine[5]);
                float geoStdDevTravelTime = Float.parseFloat(nextLine[6]);

                // Create a new uber trip
                UberTrip trip = new UberTrip(sourceId, destinationID, hourOfDay, meanTravelTime,
                                             stdDevTravelTime, geoMeanTravelTime,
                                             geoStdDevTravelTime);

                // Store first trip
                if (stack.isEmpty()) {
                    firstTrip = trip;
                }

                // Push the current trip onto the stack
                stack.push(trip);

                // Add the current trip to the queue
                queue.add(trip);

                // Keep track of the current trip added to the stack
                lastTrip = trip;
            }

        } catch (IOException e) {
            e.printStackTrace();
        }

        long finish = System.currentTimeMillis();
        long timeElapsed = finish - start;
        System.out.println("ELAPSED TIME: " + timeElapsed / 1000.0 + " seconds");

    }

    /**
     * Returns a stack that contains the last N trips that match the hour given by parameter.
     *
     * @param pTripNumber The number of trips to push onto the stack.
     * @param pHour       The time of trip.
     * @return Stack containing the trips.
     */
    public LinkedStack<UberTrip> findNTrips(int pTripNumber, int pHour) {

        // Creates an iterator to iterate over the stack
        // Counter for the number of trips added to the stack
        int count = 0;

        // A stack used to store the trips that match the criteria
        LinkedStack<UberTrip> tempStack = new LinkedStack<>();
        while (!stack.isEmpty() && count < pTripNumber) {
            UberTrip tempTrip = stack.pop();
            if (tempTrip.getHourOfDay() == pHour) {
                tempStack.push(tempTrip);
                count++;
            }
        }
        return tempStack;
    }

    public LinkedQueue<UberTrip> findLargestCluster() {

        LinkedQueue<UberTrip> finalQueue = new LinkedQueue<>();
        LinkedQueue<UberTrip> tempQueue = new LinkedQueue<>();

        UberTrip currentTrip;
        int currentHour;
        int previousHour = 0;
        boolean addFirst = false;

        while (!queue.isEmpty()) {

            currentTrip = queue.peek().getItem();

            currentHour = currentTrip.getHourOfDay();

            if (currentHour >= previousHour) {
                tempQueue.add(queue.poll().getItem());
                previousHour = currentTrip.getHourOfDay();
            }
            else {

                if (tempQueue.size() > finalQueue.size()) {
                    finalQueue.flushQueue();
                    while (!tempQueue.isEmpty()) {
                        finalQueue.add(tempQueue.poll().getItem());
                    }

                    tempQueue.flushQueue();
                    previousHour = 0;
                }
            }

        }
        return finalQueue;
    }
}

