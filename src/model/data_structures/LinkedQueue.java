/*
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * University of the Andes
 * Department of Systems Engineering
 * Licensed under Academic Free License version 2.1
 * Lab 2: Stacks & Queues
 * Author: Steven Rodriguez
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 */
package model.data_structures;

// -------------------------------------------------------------
// Imports
// -------------------------------------------------------------


/**
 * A linked list implementation of a generic queue.
 *
 * @param <T> Generic type.
 */
public class LinkedQueue<T> implements IQueue<T> {

    // -------------------------------------------------------------
    // Attributes
    // -------------------------------------------------------------

    /**
     * Represents the top node of the queue
     */
    private Node<T> header;

    /**
     * Represent the tail of the queue
     */
    private Node<T> tail;

    /**
     * Represent the actual size of the queue;
     */
    private int size;

    // -------------------------------------------------------------
    // Constructors
    // -------------------------------------------------------------

    /**
     * Constructs a new, empty Queue.
     */

    public LinkedQueue() {
        header = null;
        tail = null;
        size = 0;
    }


    /**
     * Return the size of the Queue
     *
     * @return int size
     */

    public int size() {
        return size;
    }


    /**
     * Return if the Queue is empty or not.
     *
     * @return False if the Queue is'nt empty.
     */

    public boolean isEmpty() {
        return header == null;
    }

    /**
     * Add a element at the tail of queue.
     */

    public void add(T item) {

        Node<T> n = new Node<T>(item);
        if (header == null)
            header = n;
        else
            tail.setNextNode(n);
        tail = n;
        size++;

    }

    /**
     * Is used to view the head of queue without removing it.
     *
     * @return null is the Queue is Empty.
     */

    public Node<T> peek() {
        return header;
    }

    /**
     * Remove and return the head of the queue,
     *
     * @return null is the Queue is Empty
     */

    public Node<T> poll() {

        if (header == null)
            return null;
        Node<T> delete = header;
        header=header.getNext();
        return delete;
    }

    /**
     * Flushes the queue.
     */
    public void flushQueue() {
        while(!isEmpty()){
            poll();
        }
        size = 0;
    }
}
