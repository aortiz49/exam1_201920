/*
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * University of the Andes
 * Department of Systems Engineering
 * Licensed under Academic Free License version 2.1
 * Lab 2: Stacks & Queues
 * Author: Andy Ortiz & Steven Rodriguez
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 */
package model.data_structures;

/**
 * This node represents an Uber trip in Bogota.
 *
 * @param <T> Element of the node.
 */
public class Node<T> {
    // -------------------------------------------------------------
    // Attributes
    // -------------------------------------------------------------
    /** Reference to the next node in the stack */
    private Node<T> next;

    /** Element contained in the node*/
    private T item;

    // -------------------------------------------------------------
    // Constructor
    // -------------------------------------------------------------

    public Node(T pItem) {
        next = null;
        item = pItem;
    }
    // -------------------------------------------------------------
    // Methods
    // -------------------------------------------------------------

    /**
     * Returns the next node.
     *
     * @return The next node in the list.
     */
    Node<T> getNext() {
        return next;
    }

    /**
     * Returns the item in the node.
     *
     * @return The item in the node.
     */
    public T getItem() {
        return item;
    }

    /**
     * Sets the next node in the list.
     *
     * @param pNext The next node in the list.
     */
    void setNextNode(Node<T> pNext) {
        next = pNext;
    }
}