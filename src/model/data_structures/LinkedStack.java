/*
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * University of the Andes
 * Department of Systems Engineering
 * Licensed under Academic Free License version 2.1
 * Lab 2: Stacks & Queues
 * Author: Andy Ortiz
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 */
package model.data_structures;

import java.util.EmptyStackException;

/**
 * Linked list implementation of a generic stack.
 *
 * @param <T> Generic type.
 */
public class LinkedStack<T> implements IStack<T> {
    // -------------------------------------------------------------
    // Attributes
    // -------------------------------------------------------------

    /** The top node of the stack **/
    private Node<T> top;

    /** The current size of the stack */
    private int stackSize;

    // -------------------------------------------------------------
    // Constructors
    // -------------------------------------------------------------

    /**
     * Constructs a new, empty stack.
     */
    public LinkedStack() {
        top = null;
        stackSize = 0;
    }

    // -------------------------------------------------------------
    // Methods
    // -------------------------------------------------------------

    /**
     * Pushes a new node onto the stack.
     *
     * @param pItem Item to be pushed onto the stack.
     */
    public void push(T pItem) {
        Node<T> newNode = new Node<>(pItem);

        // Make the new node the top of the stack if the stack is empty
        if (isEmpty()) {
            top = newNode;
        }

        else {
			newNode.setNextNode(top);
			top = newNode;
        }
        stackSize++;
    }

	/**
	 * Pops off the top node of the stack;
	 * @return The node popped off the stack.
	 */
	public T pop() {
        if (isEmpty())
            throw new EmptyStackException();

        T currentTop = peek();
        Node <T> newTop = top.getNext();

        // Set the next node of the current top to null
        top.setNextNode(null);
        top = newTop;
        stackSize--;
        return currentTop;
    }

    /**
     * Gets the current size of the stack.
     * @return The size of the stack.
     */
    public int size() {
        return stackSize;
    }

    /**
     * Returns the top of the stack without popping it.
     * @return Top of the stack.
     */
    public T peek() {
        return top.getItem();
    }

    /**
     * Checks if the stack is empty.
     * @return True if empty, false if otherwise.
     */
    public boolean isEmpty() {
        return stackSize==0;
    }

    /**
     * Returns the top node of the stack.
     * @return Top node in the stack.
     */
    public Node<T> getTop() {
        return top;
    }

}

