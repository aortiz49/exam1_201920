/*
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * University of the Andes
 * Department of Systems Engineering
 * Licensed under Academic Free License version 2.1
 * Lab 2: Stacks and Queues
 * Author: Andy Ortiz
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 */

package model.data_structures;


/**
 * Represents a basic, generic stack interface.
 * @param <T> Generic type.
 */
public interface IStack<T>  {

	/**
	 * Pushes a new node onto the stack.
	 *
	 * @param pItem The node to be pushed onto the stack.
	 */
    void push(T pItem);

	/**
	 * Pops an item from the stack.
	 * @return The node that's on the top of the stack.
	 */
    T pop();

	/**
	 * Gives the size of the current stack.
	 * @return The size of the stack.
	 */
	int size();

	/**
	 * "Peeks" at the top node in the stack without popping it off the stack.
	 * @return The node you want to peek at.
	 */
	T peek();

	/**
	 * Checks if the stack is empty.
	 * @return True if the stack is empty, false if otherwise.
	 */
	boolean isEmpty();

}