/*
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * University of the Andes
 * Department of Systems Engineering
 * Licensed under Academic Free License version 2.1
 * Lab 2: Stacks & Queues
 * Author: Steven Rodriguez
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 */
package model.data_structures;

/**
 * Represents a basic, generic Queue interface.
 * @param <T> Generic type.
 */
public interface IQueue<T> {

	/**
	 * Gives the size of the current queue.
	 * @return The size of the queue.
	 */
	int size();

	/**
	 * Checks if the stack is empty.
	 * @return True if the stack is empty, false if otherwise.
	 */
	boolean isEmpty();

	/**
	 * Add a element at the tail of queue.
	 * @param item Item to add to the queue.
	 */
    void add(T item);

    /**
	 * Is used to view the head of the queue without removing it.
	 * @return The node you want to peek at.
	 */
	Node<T> peek();

	/**
	 * Remove and return the head of the queue.
	 * @return The head node of the queue .
	 */
	Node<T> poll();

}
