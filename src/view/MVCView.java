/*
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * University of the Andes
 * Department of Systems Engineering
 * Licensed under Academic Free License version 2.1
 * Lab 2: Stacks & Queues
 * Author: Andy Ortiz & Steven Rodriguez
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 */

package view;

import model.data_structures.LinkedQueue;
import model.data_structures.LinkedStack;
import model.logic.UberTrip;

/**
 * Publishes the view for the user.
 */
public class MVCView {

    // -------------------------------------------------------------
    // Methods
    // -------------------------------------------------------------

    /**
     * Prints the menu
     */
    public void printMenu() {
        System.out.println("==== MENU ====");
        System.out.println("1. Load all Uber trips into the stack and queue.");
        System.out.println("2. Print last N trips at a given hour.");
        System.out.println("3. Print the largest consecutive cluster (increasing hour).");
        System.out.println("4. Exit\n\n");
    }

    /**
     * Prints the number of total trips from the linked list.
     *
     * @param pSize The size of the linked list.
     */
    public void printTotalTrips(int pSize) {
        String formattedTripCount = String.format("%,d", pSize);
        System.out.println("Total number of Uber trips: " + formattedTripCount);
    }

    /**
     * Prints a trip from the stack.
     *
     * @param pTripToPrint The trip to be printed.
     */
    public void printTrip(UberTrip pTripToPrint) {

        System.out.println("------------------------------");
        System.out.println("origin zone: " + pTripToPrint.getSourceId());
        System.out.println("destination zone: " + pTripToPrint.getDestinationId());
        System.out.println("hour of day travel time: " + pTripToPrint.getHourOfDay());
        System.out.println("mean travel time: " + pTripToPrint.getMeanTravelTime());
        System.out.println("------------------------------\n");
    }

    /**
     * Prints a list of trips from the stack.
     *
     * @param pTripStack The stack to be printed.
     * @param pNum       The number of trips the user wants.
     */
    public void printTrips(LinkedStack<UberTrip> pTripStack, int pNum) {

        // The size of the stack used to store the consulted trips requested by the user
        int size = pTripStack.size();

        // If there are no trips
        if (size == 0)
            System.out.println("There were no trips matching this hour.");

            // If there were less trips found
        else if (pNum > size) {
            System.out.println(size + " trips found! ");
            while (!pTripStack.isEmpty()) {
                UberTrip temp = pTripStack.pop();
                System.out.println("------------------------------");
                System.out.println("origin zone: " + temp.getSourceId());
                System.out.println("destination zone: " + temp.getDestinationId());
                System.out.println("hour of day travel time: " + temp.getHourOfDay());
                System.out.println("mean travel time: " + temp.getMeanTravelTime());
                System.out.println("------------------------------\n");
            }
        }
    }

    /**
     * Prints a list of trips from the queue.
     *
     * @param pTripQueue The stack to be printed.
     */
    public void printTrips(LinkedQueue<UberTrip> pTripQueue) {

        // The size of the stack used to store the consulted trips requested by the user
        int size = pTripQueue.size();

        // If there are no trips
        if (size == 0)
            System.out.println("There were no trips matching this hour.");

        while (!pTripQueue.isEmpty()) {
            UberTrip temp = pTripQueue.poll().getItem();
            System.out.println("------------------------------");
            System.out.println("origin zone: " + temp.getSourceId());
            System.out.println("destination zone: " + temp.getDestinationId());
            System.out.println("hour of day travel time: " + temp.getHourOfDay());
            System.out.println("mean travel time: " + temp.getMeanTravelTime());
            System.out.println("------------------------------\n");
        }
    }

}



