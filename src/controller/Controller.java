/*
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * University of the Andes
 * Department of Systems Engineering
 * Licensed under Academic Free License version 2.1
 * Lab 2: Stacks & Queues
 * Authors: Andy Ortiz & Steven Rodriguez
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 */
package controller;

// -------------------------------------------------------------
// Imports
// -------------------------------------------------------------

import java.util.Arrays;
import java.util.InputMismatchException;
import java.util.Scanner;

import model.logic.MVCModel;
import view.MVCView;

/**
 * Controller class to render the view and update the model.
 */
public class Controller {
    // -------------------------------------------------------------
    // Attributes
    // -------------------------------------------------------------

    /**
     * A model.
     */
    private MVCModel model;

    /**
     * A view.
     */
    private MVCView view;

    // -------------------------------------------------------------
    // Constructor
    // -------------------------------------------------------------

    /**
     * Creates the project view and the project model
     */
    public Controller() {
        model = new MVCModel();
        view = new MVCView();
    }

    // -------------------------------------------------------------
    // Methods
    // -------------------------------------------------------------

    /**
     * Prints the user options and updates the view using the model.
     *
     * @throws InputMismatchException If the user inputs an incorrect number sequence.
     */
    public void run() throws InputMismatchException {
        try {
            Scanner reader = new Scanner(System.in);
            boolean end = false;

            while (!end) {
                view.printMenu();
                System.out.println(
                        "Enter the number corresponding to the option, the press the Return key: "
                                + "(e.g" + "., " + "1):");
                int option = reader.nextInt();
                switch (option) {

                    // Loads all the trips from the csv files
                    case 1:
                        System.out.println("\n##### Load Uber trips #####\n");
                        model.loadUberTrip();
                        view.printTotalTrips(model.size());
                        System.out.println("\nThe first trip in the stack is:");
                        view.printTrip(model.getFirstTrip());
                        System.out.println("The last trip in the stack is:");
                        view.printTrip(model.getLastTrip());
                        break;

                    // Prints N uber trips for the given hour
                    case 2:
                        System.out.println("\n##### Print Uber trips ##### ");
                        System.out.println("Please enter the number of trips:");
                        try {
                            int num = Integer.parseInt(reader.next());

                            System.out.println("Please enter the time of the trip:");
                            int time = Integer.parseInt(reader.next());
                            System.out.println("\nTrips at the given hour: ");
                            view.printTrips(model.findNTrips(num, time), num);
                        } catch (NumberFormatException e) {
                            System.out.println(Arrays.toString(e.getStackTrace()));
                        }
                        break;

                    // Prints N uber trips for the given hour
                    case 3:
                        System.out.println("\n##### Print Uber cluster ##### ");

                        System.out.println("\nTrips in the longest cluster by consecutive hours: ");
                        view.printTrips(model.findLargestCluster());
                        break;

                    // Exit the program
                    case 4:
                        end = true;
                        break;

                    // Invalid option
                    default:
                        System.out.println("\n########## \n?Invalid option !! \n##########");
                        break;
                }
            }
        } catch (InputMismatchException e) {
            System.out.println("Invalid option!");
            run();
        }
    }
}
