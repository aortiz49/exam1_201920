/*
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * University of the Andes
 * Department of Systems Engineering
 * Licensed under Academic Free License version 2.1
 * Lab 2: Stacks & Queues
 * Authors: Andy Ortiz & Steven Rodriguez
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 */
package main;

import controller.Controller;

/**
 * The main class.
 */
public class MVC {

	/**
	 * Starts the controller.
	 * @param args Arguments.
	 */
    public static void main(String[] args) {
        Controller controller = new Controller();
        controller.run();
    }
}
