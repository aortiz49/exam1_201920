/*
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * University of the Andes
 * Department of Systems Engineering
 * Licensed under Academic Free License version 2.1
 * Lab 2: Stacks & Queues
 * Author: Andy Ortiz
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 */
package test.data_structures;

// -------------------------------------------------------------
// Imports
// -------------------------------------------------------------

import static org.junit.Assert.*;

import model.data_structures.LinkedStack;
import model.data_structures.Node;
import model.logic.UberTrip;
import org.junit.Before;
import org.junit.Test;

/**
 * Test class for linked stack.
 */
public class TestLinkedStack {

    /**
     * Stack used in unit tests.
     */
    private LinkedStack<UberTrip> testStack;

    /**
     * Initial setup.
     */
    @Before
    public void setUp1() {
        testStack = new LinkedStack<>();
    }

    /**
     * Setup in which trips are added.
     */
    public void setUp2() {
        // Create test trips
        UberTrip testTrip1 = new UberTrip(1, 6, 12, 12.3f, 34.5f, 34.6f, 44.5f);
        UberTrip testTrip2 = new UberTrip(2, 6, 12, 12.3f, 34.5f, 34.6f, 44.5f);
        UberTrip testTrip3 = new UberTrip(3, 6, 12, 12.3f, 34.5f, 34.6f, 44.5f);
        UberTrip testTrip4 = new UberTrip(4, 6, 12, 12.3f, 34.5f, 34.6f, 44.5f);
        UberTrip testTrip5 = new UberTrip(5, 6, 12, 12.3f, 34.5f, 34.6f, 44.5f);

        // Push each trip to the stack
        testStack.push(testTrip1);
        testStack.push(testTrip2);
        testStack.push(testTrip3);
        testStack.push(testTrip4);
        testStack.push(testTrip5);
    }

    /**
     * Tests the linked stack constructor.
     */
    @Test
    public void testLinkedStack() {
        // Tests if stack is empty
        assertTrue(testStack.isEmpty());
    }

    /**
     * Tests the push operation of the stack.
     */
    @Test
    public void testPush() {
        setUp2();

        // Tests push
        assertEquals(5, testStack.size());
    }

    /**
     * Tests the size of the stack.
     */
    @Test
    public void testSize() {
        setUp2();

        // Tests push
        UberTrip testTrip5 = new UberTrip(5, 6, 12, 12.3f, 34.5f, 34.6f, 44.5f);
        testStack.push(testTrip5);
        assertEquals(6, testStack.size());
    }

    /**
     * Tests if the stack is empty.
     */
    @Test
    public void testisEmpty() {
        setUp2();

        testStack.pop();
        testStack.pop();
        testStack.pop();
        testStack.pop();
        testStack.pop();

        // Tests push
        assertTrue(testStack.isEmpty());
    }

    /**
     * Tests if the stack can return the top trip.
     */
    @Test
    public void testPeek() {
        setUp2();

        // Tests peek
        assertEquals(5, testStack.peek().getSourceId());
    }

    /**
     * Tests the pop operation of the stack.
     */
    @Test
    public void testPop() {
        setUp2();

        // Tests pop and getTop
        testStack.pop();
        Node<UberTrip> testTop = testStack.getTop();

        assertEquals(4, testTop.getItem().getSourceId());
    }

}
