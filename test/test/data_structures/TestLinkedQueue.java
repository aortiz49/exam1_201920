package test.data_structures;

//-------------------------------------------------------------
//Imports
//-------------------------------------------------------------

import static org.junit.Assert.*;

import model.data_structures.LinkedQueue;
import model.data_structures.Node;
import model.logic.UberTrip;
import org.junit.Before;
import org.junit.Test;

/**
 * Test class for linked queue.
 */
public class TestLinkedQueue {


	/**
	 * Queue used in unit tests.
	 */
	private LinkedQueue<UberTrip> testQueue;

	/**
	 * Initial setup.
	 */
	@Before
	public void setUp1() {
		testQueue = new LinkedQueue<>();
	}

	/**
	 * Setup in which trips are added.
	 */
	public void setUp2() {

		// Create test trips
		UberTrip testTrip1 = new UberTrip(1, 6, 12, 12.3f, 34.5f, 34.6f, 44.5f);
		UberTrip testTrip2 = new UberTrip(2, 6, 12, 12.3f, 34.5f, 34.6f, 44.5f);
		UberTrip testTrip3 = new UberTrip(3, 6, 12, 12.3f, 34.5f, 34.6f, 44.5f);
		UberTrip testTrip4 = new UberTrip(4, 6, 12, 12.3f, 34.5f, 34.6f, 44.5f);
		UberTrip testTrip5 = new UberTrip(5, 6, 12, 12.3f, 34.5f, 34.6f, 44.5f);

		// Create test trips

		testQueue.add(testTrip1);
		testQueue.add(testTrip2);
		testQueue.add(testTrip3);
		testQueue.add(testTrip4);
		testQueue.add(testTrip5);

		System.out.println("");
	}

	/**
	 * Tests the linked queue constructor.
	 */
	@Test
	public void testQueueStack() {
		// Tests if stack is empty
		assertTrue(testQueue.isEmpty());
	}


	/**
	 * Tests the linked queue add.
	 */
	@Test
	public void testAdd() {
		setUp2();

		// Tests add
		assertEquals(5, testQueue.size());
	}

	/**
	 * Tests the size of the Queue
	 */
	@Test
	public void testSize() {
		setUp2();

		// Tests push
		UberTrip testTrip5 = new UberTrip(5, 6, 12, 12.3f, 34.5f, 34.6f, 44.5f);
		testQueue.add(testTrip5);
		assertEquals(6, testQueue.size());
	}

	/**
	 * Tests if the queue is empty.
	 */
	@Test
	public void testIsEmpty() {
		setUp2();

		testQueue.poll();
		testQueue.poll();
		testQueue.poll();
		testQueue.poll();
		testQueue.poll();

		// Tests push
		assertTrue(testQueue.isEmpty());
	}

	/**
	 * Tests the queue peek .
	 */

	@Test
	public void testPeek(){
		setUp2();

		// Tests case 1= Queue with elements.

		Node<UberTrip> x= testQueue.peek();
		System.out.println(x.getItem().getSourceId());

		assertEquals(1,x.getItem().getSourceId());

		// Tests case 2= Queue without elements.

		testQueue.poll();
		testQueue.poll();
		testQueue.poll();
		testQueue.poll();
		testQueue.poll();

		assertEquals(null,testQueue.peek());

	}

	/**
	 * Tests the poll of the Queue
	 */
	@Test
	public void testPoll() {
		setUp2();

		testQueue.poll();

		assertEquals(2,testQueue.peek().getItem().getSourceId());

	}






}
